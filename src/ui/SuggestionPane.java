package ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;

public class SuggestionPane extends JTextField {
	
	UI _ui;
	int _state = 0;
	int _acOrSu = 5;
	ArrayList<String> _inputs = new ArrayList<String>();
	
	/**
	 * Necessary because inside an anon. inner class, 'this' will not refer to the SuggestionPane.
	 */
	SuggestionPane _self = this;
	
	public SuggestionPane(UI ui) {
		
		_ui = ui;
		this.setEnabled(false);
		
		this.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				String input = e.getActionCommand();
				
				switch (_state) {
				
					case 0:
						
						if(checkWeapon(input)==true&&_state==0){
						_state++;
						_self.setText("");
						_inputs.add(input);
						_ui.displayInstruction("Please enter the room.");
						
						}
						else if(checkWeapon(input)==false&&_state==0) {
							_ui.displayMessage("Please enter one of these weapons: Candlestick, Poision, Rope, Gloves, Horseshoe, Knife, Lead Pipe, Revolver, or Wrench.");
							_self.setText("");
							_state=0;
						}
					case 1:
						
						if(checkRoom(input)==true&&_state==1){
							
						
						
						_state++;
						_self.setText("");
						_inputs.add(input);
						_ui.displayInstruction("Please enter the player.");
						break;
						}
						else if(checkRoom(input)==false&&_state==1){
							_ui.displayMessage("Please enter one of these rooms: Study, Hall, Lounge, Dining Room, Kitchen, Ballroom, Convservatory, Billiard Room, Library");
							_self.setText("");
							_state =1;
						}
					case 2:
						
						if(checkPlayer(input)==true&&_state==2){
						
						_state = 0;
						_self.setText("");
						_inputs.add(input);
						//ui.makeSuggestion
						_self.setEnabled(false);
						
						// submit the suggestion
						if(_acOrSu==0){
						_ui.passSuggestion(_inputs);
						}
						if(_acOrSu==1){
							_ui.passAccusation(_inputs);
						}
					
						break;}
						else if(checkPlayer(input)==false&&_state==2){
							_ui.displayMessage("Please enter one of these people: Ms. Scarlet, Col. Mustard, Mrs. White. Rev. Green, Mrs. Peacock, Prof. Plum");
							_self.setText("");
							_state =2;
						}
				
				}
				
			}
		});
	}
	
	public void grabSuggestionInput() {
		_acOrSu =0;
		_state = 0;
		_inputs = new ArrayList<String>();
		this.setEnabled(true);
		_ui.displayInstruction("Please enter the weapon.");
	}
	public void grabAccusationInput() {
		_acOrSu =1;
		_state = 0;
		_inputs = new ArrayList<String>();
		this.setEnabled(true);
		_ui.displayInstruction("Please enter the weapon.");
	}
	public static String simpleString(String input){
		String output="";
		for(int i=0;i<input.length();i++){
			if(input.charAt(i)!=' '){
				output=output+input.charAt(i);
			}
			
		}
		output =output.toLowerCase();
		return output;
	}
	public boolean checkWeapon(String input){
		input = simpleString(input);
		
		if(input.equals( "poison")){
			return true;
		}
		else if(input.equals( "rope")){
			return true;
		}
		else if(input.equals( "gloves")){
			return true;
		}
		else if(input.equals( "horseshoe")){
			return true;
		}
		else if(input.equals( "knife")){
			return true;
		}
		else if(input.equals( "leadpipe")){
			return true;
		}
		else if(input.equals( "revolver")){
			return true;
		}
		else if(input.equals( "wrench")){
			return true;
		}
		
		
		else{
		return false;
		}
	}
	public boolean checkRoom(String input){
		input = simpleString(input);
		 if(input.equals( "study")){
			return true;
		}
		else if(input.equals( "hall")){
			return true;
		}
		else if(input.equals( "lounge")){
			return true;
		}
		else if(input.equals( "diningroom")){
			return true;
		}
		else if(input.equals( "kitchen")){
			return true;
		}
		else if(input.equals( "ballroom")){
			return true;
		}
		else if(input.equals( "conservatory")){
			return true;
		}
		else if(input.equals( "billiardroom")){
			return true;
		}
		else if(input.equals( "library")){
			return true;
		}
		
		else{
		return false;
		}
	}
	public boolean checkPlayer(String input){
		input = simpleString(input);
		if(input.equals("ms.scarlet")){
			return true;
		}
		else if(input.equals( "col.mustard")){
			return true;
		}
		else if(input.equals( "mrs.white")){
			return true;
		}
		else if(input.equals( "rev.green")){
			return true;
		}
		else if(input.equals( "mrs.peacock")){
			return true;
		}
		else if(input.equals( "prof.plum")){
			return true;
		}
		
		
		else{
		return false;
		}
	}
}
