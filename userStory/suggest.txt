suggest.txt

Explain how to see that a player can enter a suggestion:

The suggestion button is always at the bottom of the GUI but it doesn't do anything unless you are in a 
room.

--------------------------------------------------------------------------------------------------------------

Explain how to see that a suggestion will have to include a weapon & person and forces the player to use 
the room they are in:

The player isn't allowed to pick a room card because it takes the room that the player is currently in.
Then the player selects one player card and one weapon card.

--------------------------------------------------------------------------------------------------------------

Explain how to see that a suggestion can only be made when a player is in a room:

The sugestion button is always at the bottom but it does nothing if the player isn't  in a room.
